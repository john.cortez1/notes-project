import 'dart:convert';

class Note{
  int? id;
  String? subject, content;

  Note({this.id, this.subject, this.content});

  factory Note.fromJson(Map<String, dynamic> jsonData){
    return Note(
      id: jsonData['id'],
      subject: jsonData['subject'],
      content: jsonData['content']
    );
  }

  static Map<String, dynamic> toMap (Note note) => {
    'id' :note.id,
    'subject': note.subject,
    'content': note.content
  };

  static String encode(List<Note> notes) => json.encode(notes
    .map<Map<String, dynamic>>((note) => Note.toMap(note))
    .toList());

  static List<Note> decode(String notes) =>
      (json.decode(notes) as List<dynamic>)
      .map<Note>((item) => Note.fromJson(item))
      .toList();
}
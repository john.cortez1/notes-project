import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../classes/my_shared_pref.dart';
import '../classes/note.dart';

class MyHomePage extends StatefulWidget {
  ///Default
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  ///Page Title
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ///List of Created Notes retrieved from Shared Preferences
  List<Note> notes = [];

  @override
  void initState(){
    super.initState();

    //initState is used to call onLoad to populate the Home with notes
    onLoad();
  }

  void onLoad() async{
    //Checks if notes exists in SharedPref then retrieves data if it exists
    if (await MySharedPreferences.instance.containsKey('notes')) {
      final getNotes = await MySharedPreferences.instance.getStringData('notes');
      setState(() { notes = Note.decode(getNotes); });
    }
  }

  void onDelete(int noteId) async{
    //Deletes the note using passed id
    notes.removeWhere((note) => note.id == noteId);

    //Saves the list to Shared Preferences then calls onLoad() to reload the list
    final String encodeData = Note.encode(notes);
    await MySharedPreferences.instance.setStringData('notes', encodeData);

    onLoad();
  }

  void onNavigate(int? noteId) async{
    //noteId is nullable for creating new notes. noteId with value is used to retrieve the existing note for editing
    final isSaved =
      await Navigator.pushNamed(context, '/edit', arguments: noteId);

    //isSaved receives popped value from Edit page and determines whether calling onLoad() is necessary or not
    if (isSaved.toString().isNotEmpty) { onLoad(); }
  }

  ///Card Template for displaying each notes
  Widget noteCardTemplate(notes){
    return Container(
      padding: const EdgeInsets.only(top:10.0),
      child: Slidable(
        groupTag: '0',
        key: ValueKey(notes.id),
        startActionPane: ActionPane(
          motion: const ScrollMotion(),
          children: [
            SlidableAction(
              onPressed: (_) => onDelete(notes.id),
              //Haven't found a way to implement the theme in custom_theme class
              backgroundColor: const Color(0xFFFE4A49),
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: 'Delete',
            ),
          ],
        ),
        child: ListTile(
          onLongPress: () => onNavigate(notes.id),
          title: Text(
            notes.subject,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          subtitle: Container(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              notes.content,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: SlidableNotificationListener(
          child: Column(
            //Maps all existing data in notes to the Card Template
            children: notes.map((note) => noteCardTemplate(note)).toList(),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){ onNavigate(null); },
        child: const Icon(Icons.add),
      ),
    );
  }
}

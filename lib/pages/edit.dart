import 'package:flutter/material.dart';

import '../classes/my_shared_pref.dart';
import '../classes/note.dart';

class Edit extends StatefulWidget {
  const Edit({Key? key}) : super(key: key);

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  ///Used to store the notes
  List<Note> notes = [];

  ///Holds the passed ID for editing or a null value for creating a new note
  late int? getPassedId;
  ///Holds the index used to retrieve and edit the notes
  late final int index;

  ///Temporarily holds the subject
  String getSubject = '' ;
  ///Temporarily holds the content
  String getContent = '' ;

  ///Controller for Subject Text Field
  final _subjectTextController = TextEditingController();
  ///Controller for Content Text Field
  final _contentTextController = TextEditingController();

  //initState used to call onLoad() and setup the listener for the text fields
  @override
  void initState() {
    super.initState();
    onLoad();
    _subjectTextController.addListener(handleSubjectTextChange);
    _contentTextController.addListener(handleContentTextChange);
  }

  void onLoad() async {
    if(await MySharedPreferences.instance.containsKey('notes')) {
      final getNotes = await MySharedPreferences.instance.getStringData('notes');
      notes = Note.decode(getNotes);

      //Checks if getPassedId has data, if so then populate the text fields
      if(getPassedId != null) {
        //Retrieves the index of the note ID passed from getPassedId
          index = notes.indexWhere((note) => note.id == getPassedId);

        //Populates the text field with data
        getSubject = notes[index].subject.toString();
        _subjectTextController.text = getSubject;

        getContent = notes[index].content.toString();
        _contentTextController.text = getContent;
      }

    }
  }

  void handleSubjectTextChange() {
    setState(() {
      getSubject = _subjectTextController.text.trim();
    });
  }

  void handleContentTextChange() {
    setState(() {
      getContent = _contentTextController.text.trim();
    });
  }

  //Handles the back button action for auto saving
  void handleBackButton() async {
    if(getSubject.isNotEmpty || getContent.isNotEmpty) {
      //For editing notes
      if(getPassedId != null) {
        notes[index] = (Note(
          id: notes[index].id, subject: getSubject, content: getContent));
      }
      //For newly created notes
      else {
        late final int newId;

        if (notes.isEmpty) {
          newId = 0; }
        else {
          newId = notes[notes.length-1].id! + 1; }

        notes.add(Note(
          id: newId, subject: getSubject, content: getContent));
      }

      //Saves the notes to Shared Preferences
      final String encodeData = Note.encode(notes);
      await MySharedPreferences.instance.setStringData('notes', encodeData);
      //Pop has content 'saved' for setState when it returns to home
      Navigator.pop(context, 'saved');
    }
    else { Navigator.pop(context, null); }
  }

  @override
  void dispose() {
    _subjectTextController.dispose();
    _contentTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //Retrieves the passed ID from home
    getPassedId = ModalRoute.of(context)!.settings.arguments as int?;
    return WillPopScope(
      onWillPop: () async {
        handleBackButton();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(title: const Text(''),),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget> [
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 18, 10, 3),
              child: TextField(
                controller: _subjectTextController,
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                  hintText: 'Title',
                  hintStyle: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
                child: TextField(
                  controller: _contentTextController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  style: Theme.of(context).textTheme.bodyText2,
                  decoration: InputDecoration(
                    hintText:  'Note',
                    hintStyle: Theme.of(context).textTheme.subtitle2,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
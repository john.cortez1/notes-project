import 'package:flutter/material.dart';

import '../pages/edit.dart';
import '../pages/home.dart';
import '../theme/custom_theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  ///Default
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Notes Demo',
      home: const MyHomePage(title: 'Notes'),
      theme: CustomTheme.darkTheme,
      routes: {
        '/edit': (BuildContext context) => const Edit(),
      },
    );
  }
}


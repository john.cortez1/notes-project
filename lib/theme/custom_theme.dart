import 'package:flutter/material.dart';

class CustomTheme {
  static ThemeData get darkTheme {
    return ThemeData(
      primaryColor: Colors.grey,
      appBarTheme: AppBarTheme(
        backgroundColor: Colors.grey[800],
      ),
      scaffoldBackgroundColor: Colors.grey[900],
      fontFamily: 'Montserrat',

      textTheme: const TextTheme(
        subtitle1: TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.bold,
        ),
        subtitle2: TextStyle(
          color: Colors.grey,
        ),
        bodyText1: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
        bodyText2: TextStyle(
          color: Colors.white,
        )
      ),

      inputDecorationTheme: const InputDecorationTheme(
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
      ),

      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.grey[850],
      ),

      listTileTheme: ListTileThemeData(
        minVerticalPadding: 10.0,
        tileColor: Colors.grey[850],
      ),

    );
  }
}